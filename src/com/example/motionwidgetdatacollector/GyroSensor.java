package com.example.motionwidgetdatacollector;

import java.io.*;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

public class GyroSensor implements GyroRecorder{
	
	public SensorManager gyrosensor; 
	public SensorEventListener gyrorecord;
	public SensorEvent gyrodelta;
	public Sensor gyroscope;
	public File datafile;
	private StringBuffer recordeddata;
	private String comma;
	private BufferedWriter data_writer;
	
	public GyroSensor() {
		recordeddata = new StringBuffer();
		comma = new String(",");
 	}
	
	public void startRecord(Sensor gyroscope) {
		System.out.println("Recording");
		
	}

	@Override
	public void stopRecord() {
		gyrosensor.unregisterListener(gyrorecord, gyroscope);

	}

	public void sensorOnChanged(SensorEvent gyrodelta) {
		writeData();
		
		}
	public void writeData(){
		recordeddata.append(String.valueOf(gyrodelta.values[0]));
		recordeddata.append(comma);
		recordeddata.append(String.valueOf(gyrodelta.values[1]));
		recordeddata.append(comma);
		recordeddata.append(String.valueOf(gyrodelta.values[2]));
		try {
			data_writer = new BufferedWriter(new FileWriter (datafile));
			String buffer_converter = recordeddata.toString();
			data_writer.write(buffer_converter);
		} catch (IOException e) {
			e.printStackTrace();
		}
		}		

	}
