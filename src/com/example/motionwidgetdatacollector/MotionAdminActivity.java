package com.example.motionwidgetdatacollector;
import java.io.*;
import java.util.*;

import android.app.*;
import android.content.*;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.view.Menu;
import android.view.View;
import android.widget.*;
import android.hardware.*;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.*;



public class MotionAdminActivity extends Activity {
	private TextView textdisplay;
	private TextView listed_apps;
	public SensorManager gyromanager;
	public SensorEventListener gyrorecord;
	public SensorEvent gyrodelta;
	Button startDataCollection;
	public Sensor gyroscope;
	public FileWriter f;
	private boolean isrecording;
	public File directory;
	private SoundPool s;
	private AudioManager sAudioManager;
	int written = 0;
	private NotificationManager recordnotify;
	private final String SD_Card_Location = "/sdcard/rec_status.tmp";
	private PackageManager installed_apps;
	private FileWriter data_writer;
	private StringBuffer recordeddata;
	public GyroSensor gyro;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_motion_admin);
        startDataCollection = (Button) findViewById(R.id.startrecord);
        textdisplay = (TextView) findViewById(R.id.textView1);
        listed_apps = (TextView) findViewById(R.id.Installed_Apps);
        s = new SoundPool(3, AudioManager.STREAM_MUSIC, 0);
        written = s.load(this, R.raw.clong, 1);
        SensorManager gyromanager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        if (gyromanager.getSensorList(Sensor.TYPE_GYROSCOPE).size()!=0){
        	Sensor gyroscope = gyromanager.getSensorList(Sensor.TYPE_GYROSCOPE).get(0);
        	gyromanager.registerListener(gyrorecord, gyroscope, 0);
        }
        written = s.load(this, R.raw.clong, 1);
        recordnotify = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        gyro = new GyroSensor();
        startDataCollection.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(written !=0){
				s.play(written, 1, 1, 0, 0, 1);
				}
				gyro.writeData();
				record_data();
			}
		});
        
        File f = new File (SD_Card_Location);
        if (! f.exists()){
        	try{
        		f.createNewFile();
        	}
        	catch (IOException e) {
        		e.printStackTrace();
        	}
        }
          
        
        
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_motion_admin, menu);
        return true;
    }
    
    public void record_data(){
    	if (isrecording){
    		isrecording=false;
    	}
    	
    	else {
    		
    		try {
    			data_writer = new FileWriter(SD_Card_Location, false);
    			data_writer.write(recordeddata.toString() + "/n");
			} 
    		catch (FileNotFoundException e){
    			e.printStackTrace();
    		}
    		catch (IOException e) {
				e.printStackTrace();
			}
    		  
    	}
    }
    
    public void findInstalledApps(){
    	installed_apps = getPackageManager();
    	List<PackageInfo> apps = installed_apps.getInstalledPackages(0);

    	
    		
    	}
    	
    	
    }


	



