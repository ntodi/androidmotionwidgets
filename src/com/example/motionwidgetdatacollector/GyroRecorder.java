package com.example.motionwidgetdatacollector;

import android.hardware.Sensor;
import android.hardware.SensorEvent;

public interface GyroRecorder {

public void startRecord(Sensor gyroscope);

public void sensorOnChanged(SensorEvent gyrodelta);

public void stopRecord();



}